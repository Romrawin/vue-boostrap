module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/~cs62160067/learn_bootstrap/'
      : '/'
}
